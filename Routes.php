<?php
    return [
        App\Core\Route::get('|^add/?$|', 'App', 'getAdd'),
        App\Core\Route::post('|^add/?$|', 'App', 'postAdd'),
        App\Core\Route::get('|^delete/([0-9]+)/?$|', 'App', 'getDelete'),
        App\Core\Route::post('|^delete/([0-9]+)/?$|', 'App', 'postDelete'),
        App\Core\Route::get('|^edit/([0-9]+)/?$|', 'App', 'getEdit'),
        App\Core\Route::post('|^edit/([0-9]+)/?$|', 'App', 'postEdit'),
        App\Core\Route::any('|^.*$|', 'App', 'home')
    ];