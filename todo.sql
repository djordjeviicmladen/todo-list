/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MariaDB
 Source Server Version : 100135
 Source Host           : localhost:3306
 Source Schema         : todo

 Target Server Type    : MariaDB
 Target Server Version : 100135
 File Encoding         : 65001

 Date: 13/09/2018 11:18:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for todo
-- ----------------------------
DROP TABLE IF EXISTS `todo`;
CREATE TABLE `todo`  (
  `todo_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `todo_post` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`todo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of todo
-- ----------------------------
INSERT INTO `todo` VALUES (29, 'ToDo');
INSERT INTO `todo` VALUES (30, 'List');
INSERT INTO `todo` VALUES (31, 'Project');

SET FOREIGN_KEY_CHECKS = 1;
