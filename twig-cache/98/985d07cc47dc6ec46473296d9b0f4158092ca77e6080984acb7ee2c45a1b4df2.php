<?php

/* App/postAdd.html */
class __TwigTemplate_49038936400f82a95c5b68ac3ef7a56f5e33173a7cf617aa1d4957a0b76f9c1b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("Public/index.html", "App/postAdd.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Public/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"card\" style=\"width: 30rem; left: 30%;\">
    <ul class=\"list-group list-group-flush\">
        <li class=\"list-group-item\">";
        // line 6
        echo twig_escape_filter($this->env, ($context["message"] ?? null), "html", null, true);
        echo "</li>
        <li class=\"list-group-item\">Click on the button to return to add more items.<a href=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "add\" 
            class=\"card-link fas fa-plus\" style=\"margin-left: 15px;\"></a></li>
        <li class=\"list-group-item\">Click on the button to return to see all the items.<a href=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" 
            class=\"card-link fas fa-home\" style=\"margin-left: 15px;\"></a></li>
    </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "App/postAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 9,  43 => 7,  39 => 6,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "App/postAdd.html", "/opt/lampp/htdocs/Views/App/postAdd.html");
    }
}
