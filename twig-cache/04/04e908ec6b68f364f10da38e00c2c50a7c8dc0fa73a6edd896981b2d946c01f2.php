<?php

/* Public/index.html */
class __TwigTemplate_ccdd688fb930f05002dc345c3d63ec02a754531d9f5db7c1555172daaac9b75c extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<html>
    <head>
        <title>ToDo - ";
        // line 4
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta charset=\"utf-8\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.3.1/css/all.css\" integrity=\"sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "Static/css/main.css\">
        <link rel=\"icon\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "Static/icon.ico\">
    </head>
    <body>
        <div class=\"container\">
            <header>
                    <nav class=\"navbar navbar-expand-lg navbar-light bg-light\">
                        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarTogglerDemo02\" aria-controls=\"navbarTogglerDemo02\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
                            <span class=\"navbar-toggler-icon\"></span>
                        </button>
                          
                        <div class=\"collapse navbar-collapse\" id=\"navbarTogglerDemo02\">
                            <ul class=\"navbar-nav mr-auto mt-2 mt-lg-0\">
                                <li class=\"nav-item\">
                                  <a class=\"nav-link\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\">Home</a>
                                </li>
                                <li class=\"nav-item\">
                                  <a class=\"nav-link\" href=\"";
        // line 25
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "add/\">Add</a>
                                </li>
                            </ul>
                        </div>
                    </nav>

                <div class=\"jumbotron\">
                    <h1 class=\"display-3 text-center\">ToDo List</h1>
                </div>
            </header>

            <main>
                ";
        // line 37
        $this->displayBlock('main', $context, $blocks);
        // line 39
        echo "            </main>
        </div>

        <script src=\"https://code.jquery.com/jquery-3.3.1.min.js\" integrity=\"sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>
    </body>
</html>";
    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        echo "Home";
    }

    // line 37
    public function block_main($context, array $blocks = array())
    {
        // line 38
        echo "                ";
    }

    public function getTemplateName()
    {
        return "Public/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 38,  96 => 37,  90 => 4,  80 => 39,  78 => 37,  63 => 25,  57 => 22,  41 => 9,  37 => 8,  30 => 4,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "Public/index.html", "/opt/lampp/htdocs/Views/Public/index.html");
    }
}
