<?php

/* App/getDelete.html */
class __TwigTemplate_baaba74b860ca04535ad53d47db8109e611ec9a0a5ced9ac0a39b92add4c7bcb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("Public/index.html", "App/getDelete.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Public/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        ob_start();
        // line 5
        echo "<form method=\"POST\">
    <ul class=\"list-group list-group-flush\">
        <li class=\"list-group-item\">
            <p>
                Are you sure you want to delete this item?
                <button type=\"submit\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "delete/";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["todo"] ?? null), "todo_id", array()), "html", null, true);
        echo "/\" class=\"btn btn-outline-danger fas fa-check\" style=\"font-size: 1em; margin-left: 2px;\"
                    data-toggle=\"tooltip\" data-placement=\"top\" title=\"Yes!\"></button>
                <a href=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "\" class=\"btn btn-outline-success fas fa-times\" style=\"font-size: 1em; margin-left: 2px;\"
                    data-toggle=\"tooltip\" data-placement=\"top\" title=\"No!\"></a>
            </p>
        </li>
    </ul>
</form>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "App/getDelete.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 12,  44 => 10,  37 => 5,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "App/getDelete.html", "/opt/lampp/htdocs/Views/App/getDelete.html");
    }
}
