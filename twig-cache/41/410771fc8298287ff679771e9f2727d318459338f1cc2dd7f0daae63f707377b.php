<?php

/* App/home.html */
class __TwigTemplate_087318a359b6a7f1c0b45ed21a250a6908c5b003fc99c1886a5a0f2afb232c9a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("Public/index.html", "App/home.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Public/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        ob_start();
        // line 5
        echo "<ul class=\"list-group list-group-flush\">
    ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["todos"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["todo"]) {
            // line 7
            echo "    <li class=\"list-group-item\" style=\"font-size: 1em;\">
        ";
            // line 8
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["todo"], "todo_post", array()), "html", null, true);
            echo "
        <a href=\"";
            // line 9
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "delete/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["todo"], "todo_id", array()), "html", null, true);
            echo "/\" class=\"fas fa-trash-alt\" style=\"font-size: 1em; margin-left: 10%;\" 
            data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"></a>
        <a href=\"";
            // line 11
            echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
            echo "edit/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["todo"], "todo_id", array()), "html", null, true);
            echo "/\" class=\"far fa-edit\" style=\"font-size: 1em; margin-left: 1%;\" 
            data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"></a>
    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['todo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 15
        echo "</ul>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "App/home.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 15,  58 => 11,  51 => 9,  47 => 8,  44 => 7,  40 => 6,  37 => 5,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "App/home.html", "/opt/lampp/htdocs/Views/App/home.html");
    }
}
