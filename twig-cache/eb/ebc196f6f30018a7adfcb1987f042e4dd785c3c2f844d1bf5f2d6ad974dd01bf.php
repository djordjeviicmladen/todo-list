<?php

/* App/getAdd.html */
class __TwigTemplate_7dd96fd51fbc9ccc04fab841fe9b2e6b6b1a10325e126a4efa9c55efaaeca2fc extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("Public/index.html", "App/getAdd.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Public/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
    <form action=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["BASE"] ?? null), "html", null, true);
        echo "add\" method=\"POST\">
        <div class=\"card\" style=\"left: 165%;\">
            <div class=\"card-header\">
                ToDo Add
            </div>

            <div class=\"card-body\">
                <div class=\"form-group\">
                    <label for=\"todo_post\">Item:</label>
                    <input type=\"text\" id=\"todo_post\" name=\"todo_post\" required class=\"form-control\"
                        placeholder=\"Enter the name of the item.\">
                </div>
            </div>

            <div class=\"card-footer text-right\">
                <button type=\"submit\" class=\"btn btn-outline-primary fas fa-check\" 
                    data-toggle=\"tooltip\" data-placement=\"top\" title=\"Add\"></button>
            </div>
        </div>
    </form>
</div>

";
    }

    public function getTemplateName()
    {
        return "App/getAdd.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 6,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "App/getAdd.html", "/opt/lampp/htdocs/Views/App/getAdd.html");
    }
}
