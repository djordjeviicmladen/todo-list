<?php

/* App/getEdit.html */
class __TwigTemplate_7be2e9a8737e90d5fb11ac57002ed6db58a40bf62f5e75de22d7dc5f5ebf59e5 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("Public/index.html", "App/getEdit.html", 1);
        $this->blocks = array(
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Public/index.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_main($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
    <div class=\"align-item-center\">
        <form method=\"POST\">
            <div class=\"card\" style=\"left: 165%;\">
                <div class=\"card-header\">
                    ToDo Edit
                </div>

                <div class=\"card-body\">
                    <div class=\"form-group\">
                        <label for=\"todo_post\">Item:</label>
                        <input type=\"text\" id=\"todo_post\" name=\"todo_post\" required class=\"form-control\"
                            placeholder=\"Enter a new item name.\">
                    </div>
                </div>

                <div class=\"card-footer text-right\">
                    <button type=\"submit\" class=\"btn btn-outline-primary fas fa-check\" 
                        data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"></button>
                </div>
            </div>
        </form>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "App/getEdit.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "App/getEdit.html", "/opt/lampp/htdocs/Views/App/getEdit.html");
    }
}
