<?php
    namespace App\Controllers;

    use App\Core\Controller;
    use App\Models\TodoModel;
    use App\Core\Validators\StringValidator;

    class AppController extends Controller {
        public function home() {
            $todoModel = new TodoModel($this->getDatabaseConnection());
            $todos = $todoModel->getAll();
            $this->set('todos', $todos);
        }

        public function getAdd() {
            
        }

        public function postAdd() {
            $todo_post  = \filter_input(INPUT_POST, 'todo_post', FILTER_SANITIZE_STRING);

            $validItem = (new StringValidator())
                ->setMinLength(2)
                ->setMaxLength(250)
                ->isValid($todo_post);

            if ( !$validItem) {
                $this->set('message', 'There was an error: the item is not in the correct format.');
                return;
            }

            $todoModel = new TodoModel($this->getDatabaseConnection());

            $todoPost = $todoModel->getByFieldName('todo_post', $todo_post);
            if ($todoPost) {
                $this->set('message', 'There was an error: There is an item under that name already.');
                return;
            }

            $todoId = $todoModel->add([
                'todo_post'      => $todo_post,
            ]);

            if (!$todoId) {
                $this->set('message', 'There was an error: It was not possible to add an item successfully.');
                return;
            }

            $this->set('message', 'A new item has been added.');
        }

        public function getDelete($todoId) {
            $todoModel = new TodoModel($this->getDatabaseConnection());
            $todo = $todoModel->getById($todoId);

            if (!$todo) {
                $this->redirect(\Configuration::BASE);
            }

            $this->set('todo', $todo);

            return $todoModel;
        }

        public function postDelete($todoId) {
            $todoModel = $this->getDelete($todoId);

            $todoModel->deleteById($todoId);

            $this->redirect(\Configuration::BASE);
        }

        public function getEdit($todoId) {
            $todoModel = new TodoModel($this->getDatabaseConnection());
            $todo = $todoModel->getById($todoId);

            if (!$todo) {
                $this->redirect(\Configuration::BASE);
            }

            $this->set('todo', $todo);

            return $todoModel;
        }

        public function postEdit($todoId) {
            $todoModel = $this->getEdit($todoId);

            $todo_post = filter_input(INPUT_POST, 'todo_post', FILTER_SANITIZE_STRING);

            $todoModel->editById($todoId, [
                'todo_post' => $todo_post
            ]);

            $this->redirect(\Configuration::BASE);
        }
    }