<?php
    final class Configuration {
        const BASE = 'http://localhost/';

        const DATABASE_HOST = 'localhost';
        const DATABASE_USER = 'root';
        const DATABASE_PASS = '';
        const DATABASE_NAME = 'todo';
    }
