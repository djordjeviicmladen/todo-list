<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Core\Validators\NumberValidator;
    use App\Core\Validators\StringValidator;

    class TodoModel extends Model {
        protected function getFields(): array {
            return [
                'todo_id'      => new Field((new NumberValidator())->setIntegerLength(11), false),
                'todo_post'    => new Field((new StringValidator())->setMaxLength(64*1024))
            ];
        }
    }